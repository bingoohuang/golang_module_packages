package foo

import (
	"fmt"

	"gitlab.com/juanpabloaj/mod_pkg/bar"
)

// Config represent config of foo
type Config struct{}

// Show return foo string
func Show() string {
	fmt.Println(bar.Confg{})
	return "foo"
}
