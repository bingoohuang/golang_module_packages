package main

import (
	"fmt"

	"gitlab.com/juanpabloaj/mod_pkg/bar"
	"gitlab.com/juanpabloaj/mod_pkg/foo"
)

func show() string {
	return "main"
}

func main() {
	fmt.Println("hello from", show())
	fmt.Println("hello from", bar.Show())
	fmt.Println("hello from", foo.Show())
}
