package main

import "testing"

func TestMain(t *testing.T) {
	if show() != "main" {
		t.Errorf("expected main got other thing")
	}
}
