
# go module and sub-packages example

## Build

Run all tests

    go test ./...

Build

    go build ./...

Docker

    docker build -t mod-pkg .
    docker run mod-pkg

## References

* <https://github.com/golang/go/wiki/Modules>
